require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, el| acc + el }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| substring?(string, substring) }
end

def substring?(str, sub)
  str =~ /#{sub}/
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.chars
        .select { |char| string.chars.count(char) > 1 && char != ' ' }
        .sort
        .reduce([]) do |acc, el|
          if acc.include?(el) == false
            acc << el
          else
            acc
          end
        end
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  output = string.split(' ').reduce([]) do |longest_two, word|
    if longest_two.length < 2
      longest_two << word
    elsif longest_two[0].length < word.length
      longest_two[1] = longest_two[0]
      longest_two[0] = word
      longest_two
    elsif longest_two[1].length < word.length
      longest_two[1] = word
      longest_two
    else
      longest_two
    end
  end
  output
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').to_a.reject { |char| string.include?(char) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select { |yr| not_repeat_year?(yr) }
end

def not_repeat_year?(year)
  year.to_s.split('').uniq == year.to_s.split('')
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  output = songs.select { |song| no_repeats?(song, songs) }
                .reduce([]) do |acc, el|
                  if acc.include?(el) == false
                    acc << el
                  else
                    acc
                  end
                end
  output
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    break if songs[idx + 1].nil?
    return false if song == songs[idx + 1] && song == song_name
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  no_punc = string.split(' ').map { |word| remove_punctuation(word) }
  no_punc.reduce('') do |acc, el|
    if (acc == '' && c_distance(el) > 0) || (c_distance(el) < c_distance(acc))
      el
    else
      acc
    end
  end
end

def c_distance(word)
  c_index = 0
  word.chars.each_with_index { |char, idx| c_index = idx if char == 'c' }
  word.length - c_index
end

def remove_punctuation(word)
  word.gsub(/\W/, '')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  output = []
  range = []
  arr.each_with_index do |num, idx|
    if range == [] && num == arr[idx + 1]
      range[0] = idx
    elsif range.length == 1 && num != arr[idx + 1]
      range[1] = idx
      output << range
      range = []
    end
  end
  output
end
